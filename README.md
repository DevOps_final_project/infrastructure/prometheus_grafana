# Deploy Prometheus & Grafana on AKS

## Connect to the AKS 
~~~
az account set --subscription <subscription ID>
az aks get-credentials --resource-group <RESOURCE_GROUP_NAME> --name <AKS_CLUSTER_NAME>
~~~

## Create namespace observation
~~~
kubectl create namespace observation
~~~

## Adds the Prometheus Community Helm chart repository, which contains the Prometheus Helm chart.
~~~
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
~~~

## Adds the Grafana Helm chart repository, which contains the Grafana Helm chart
~~~
helm repo add grafana https://grafana.github.io/helm-charts
~~~

## Installs the Prometheus chart, creats a Prometheus server deployment and associates resources
~~~
helm install prometheus prometheus-community/prometheus -n observation
~~~

## Updates the local cache of available Helm charts from the added repositories
~~~
helm repo update
~~~


## check the status
~~~
kubectl get all -n observation
kubectl get pods -n observation
~~~

## Exposes the Prometheus server service as a NodePort service, allows access to the Prometheus UI and API externally
~~~
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-ext -n observation

~~~

## Installs the Grafana chart, creats a Grafana deployment and associated resources
~~~
helm install grafana grafana/grafana -n observation
~~~

## Exposes the Grafana service as a NodePort service, allows access to the Grafana UI externally
~~~
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-ext -n observation
~~~

## check the status
~~~
kubectl get all -n observation
kubectl get pods -n observation
~~~

## Retrieves the Grafana secret in YAML format, which contains the credentials for logging into the Grafana dashboard
~~~
kubectl get secret -n observation grafana -o yaml

~~~

## reveal the Grafana admin username & password(outputs will be the actual user&pass)
~~~
echo  user output from previous command | base64 -d ; echo
echo  password output from the previous commad | base64 -d ; echo
~~~

## Modifies the Grafana service configuration to change the target port to 3000. 
~~~
kubectl patch service grafana-ext --type=json -p='[{"op": "replace", "path": "/spec/ports/0/port", "value": 3000}]' -n observation

~~~

## Modifies the Prometheus server service configuration to change the target port to 9090
~~~
kubectl patch service prometheus-server-ext --type=json -p='[{"op": "replace", "path": "/spec/ports/0/port", "value": 9090}]' -n observation

~~~

## port forward and URL:
~~~
kubectl port-forward service/prometheus-server-ext 9090:9090 -n observation
http://localhost:9090/
~~~

## enter data surce in Grafana and put in the Prometheus server URL:
~~~
kubectl port-forward service/grafana-ext 3000:3000 -n observation
http://localhost:3000
~~~

## Conect Grafana & Prometheus
~~~
open grafana with:
1. http://localhost:9090
2. go to Datasources
3. In the add data source section, provide the service URL according to these commands:

4. kubectl get all -n observation
NAME                                                    READY   STATUS    RESTARTS   AGE
pod/grafana-b6fdbfc98-z85pk                             1/1     Running   0          5m15s
pod/prometheus-alertmanager-0                           1/1     Running   0          7m39s
pod/prometheus-kube-state-metrics-7897594488-vfcmw      1/1     Running   0          7m40s
pod/prometheus-prometheus-node-exporter-6kpsg           1/1     Running   0          7m40s
pod/prometheus-prometheus-node-exporter-lm4pl           1/1     Running   0          7m40s
pod/prometheus-prometheus-pushgateway-cf8d5957c-5lk5x   1/1     Running   0          7m40s
pod/prometheus-server-5c494b6d68-zvxtj                  2/2     Running   0          7m40s


5. kubectl describe pod prometheus-server-5c494b6d68-zvxtj  -n observation
scroll up and take the ip from this command

6. put the <IP from previos command:9000>
7. enable Skip TLS Verify
save it. go to the dashboard and put 3662 in the import via grafana.com
import, then scroll down to the prometheus and add the prometheus there.
~~~


# Clean UP:
## Delete the Grafana deployment and service
~~~
helm uninstall grafana -n observation
kubectl delete service grafana-ext -n observation

~~~

## Delete the Prometheus deployment and service
~~~
helm uninstall prometheus -n observation
kubectl delete service prometheus-server-ext -n observation

~~~

## Remove the Prometheus and Grafana Helm repositories
~~~
helm repo remove prometheus-community
helm repo remove grafana

~~~


